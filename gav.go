package gav

import (
	"fmt"
	"path/filepath"
	"strings"
)

const (
	defaultPackaging = "jar"
	dot              = "."
	separator        = "-"
	slash            = "/"
)

// Gav holds a Maven coordinate. Maven has no consistent naming convention for
// 'packaging', it is also called 'extension' depending on the context.
type Gav struct {
	Group, Artifact, Version, Packaging string
	// Optional
	Classifier string
}

// ConciseNotation mimics Gradle's external dependency short form,
// "group:name:version:classifier@extension"
func (a Gav) ConciseNotation() string {
	s := fmt.Sprintf("%v:%v:%v", a.Group, a.Artifact, a.Version)
	if a.Classifier != "" {
		s = fmt.Sprintf("%s:%s", s, a.Classifier)
	}
	if a.HasPackaging() {
		s = fmt.Sprintf("%s@%s", s, a.Packaging)
	}
	return s
}

// DefaultLayout returns a maven default layout for a given Gav.
// Pattern: / {{group}}[ / {{group}}]* / {{artifact}} / {{version}} /
// {{artifact}-{{version}}.{{packaging}}
func (g Gav) DefaultLayout() string {
	group := strings.ReplaceAll(g.Group, ".", slash)
	completeArtifact := g.Artifact + "-" + g.Version
	// Optionally add classifier
	if len(g.Classifier) > 0 {
		completeArtifact = completeArtifact + "-" + g.Classifier
	}
	return strings.Join([]string{
		"", // force leading slash
		group,
		g.Artifact,
		g.Version,
		completeArtifact + "." + g.Packaging}, slash)
}

// HasPackaging returns true if classifier is neither empty string nor default
// "jar" packaging.
func (g Gav) HasPackaging() bool {
	return g.Packaging != "" && g.Packaging != "jar"
}

// returns concise notation
func (g Gav) String() string {
	return g.ConciseNotation()
}

// DefaultLayout determines Maven GAV from a given filename
// <group>/<group>/<group>/<artifact>/<version>/
//      <artifact>-<version>[-classifier].<packaging>
func DefaultLayout(path string) Gav {
	dir, filename := filepath.Split(path)
	parts := strings.Split(dir, slash)
	// remove leading and trailing /
	parts = parts[1 : len(parts)-1]
	l := len(parts)

	group := strings.Join(parts[0:l-2], dot)
	artifact := parts[l-2]
	version := parts[l-1]
	packaging := filepath.Ext(filename)[1:] // strip '.'

	// extract optional classifier by removing all known elements
	classifier := filename
	classifier = strings.TrimLeft(classifier, artifact+separator+version)
	classifier = strings.TrimLeft(classifier, separator)
	classifier = strings.TrimRight(classifier, dot+packaging)

	gav := Gav{
		Group:      group,
		Artifact:   artifact,
		Version:    version,
		Classifier: classifier,
		Packaging:  packaging,
	}
	return gav
}

// NewGav returns a new GAV with default packaging and no classifier.
func NewGav(g, a, v string) Gav {
	return Gav{Group: g, Artifact: a, Version: v, Packaging: defaultPackaging}
}
