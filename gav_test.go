package gav

import "testing"

func TestConciseNotation(t *testing.T) {
	gav := Gav{Group: "g", Artifact: "a", Version: "v"}
	gavc := Gav{Group: "g", Artifact: "a", Version: "v", Classifier: "c"}
	gave := Gav{Group: "g", Artifact: "a", Version: "v", Packaging: "ear"}
	gavj := Gav{Group: "g", Artifact: "a", Version: "v", Packaging: "jar"}
	gavce := Gav{Group: "g", Artifact: "a", Version: "v", Classifier: "c",
		Packaging: "ear"}
	gavcj := Gav{Group: "g", Artifact: "a", Version: "v", Classifier: "c",
		Packaging: "jar"}

	var tests = []struct {
		gav     Gav
		concise string
	}{
		{gav, "g:a:v"},
		{gavc, "g:a:v:c"},
		{gave, "g:a:v@ear"},
		{gavj, "g:a:v"},
		{gavce, "g:a:v:c@ear"},
		{gavcj, "g:a:v:c"},
	}
	for _, tt := range tests {
		t.Run(tt.gav.String(), func(t *testing.T) {
			want := tt.concise
			got := tt.gav.ConciseNotation()
			if want != got {
				t.Fatalf("want %q but got %q", want, got)
			}
		})
	}
}

func TestIntoDefaultLayout(t *testing.T) {
	gav := NewGav("g", "a", "1.0.0")
	want := "/g/a/1.0.0/a-1.0.0.jar"
	got := gav.DefaultLayout()
	if want != got {
		t.Fatalf("want %q but got %q\n", want, got)
	}
}

func TestFromDefaultLayout(t *testing.T) {
	want := NewGav("g", "a", "1.0.0")
	got := DefaultLayout("/g/a/1.0.0/a-1.0.0.jar")
	if want != got {
		t.Fatalf("want %+v but got %+v\n", want, got)
	}
}

func TestDeepGroup(t *testing.T) {
	want := NewGav("org.mothercompany.subsidiary.team.project", "a1", "1.0.0")
	got := DefaultLayout(want.DefaultLayout())
	if want != got {
		t.Fatalf("want %+v but got %+v\n", want, got)
	}
}

func TestClassifier(t *testing.T) {
	want := Gav{"org.mothercompany.subsidiary.team.project", "a1", "1.0.0", "jar", "sources"}
	got := DefaultLayout(want.DefaultLayout())
	if want != got {
		t.Fatalf("want %+v but got %+v\n", want, got)
	}
}
